<!DOCTYPE html>
<?php 

$mode = 'debug';
//$mode = 'prod';

$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

if(isset($_COOKIE['lang'])){
  $lang = $_COOKIE['lang'];
}
if(isset($_GET['lang'])){
  $get = $_GET['lang'];
  SetCookie('lang',$get,time()+360000);
  $lang = $get;
}
if (isset($_GET['r']))
{
$get_r = $_GET['r'];
  SetCookie('ref',$get_r,time()+360000);
}


if ($mode == 'debug') {

    $file = './lang/'.$lang.'.php';
    
    if (file_exists($file))
       include($file);
    else 
       include('./assets/lang/en.php');
    
    include('./assets/landing.php');
} else {
    
    $file = './dist/pages/'.$lang.'.php';
    
    if (file_exists($file))
       include($file);
    else 
       include('./dist/pages/en.php');
}

?>

