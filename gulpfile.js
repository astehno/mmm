'use strict';

var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var php2html = require("gulp-php2html");
var rename = require("gulp-rename");
const imagemin = require('gulp-imagemin');




// Set the browser that you want to supoprt
const AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

// Gulp task to minify CSS files
gulp.task('styles', function () {
  return gulp.src('./assets/css/style.css')
    // Compile SASS files
    .pipe(sass({
      outputStyle: 'nested',
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    // Auto-prefix css styles for cross browser compatibility
    .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    // Minify the file
    .pipe(csso())
    // Output
    .pipe(gulp.dest('./dist/css'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {
  return gulp.src('./assets/js/*.js')
    // Minify the file
    .pipe(uglify())
    // Output
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./dist/js'))
});



// Gulp task to minify HTML files
gulp.task('pages', function() {
  return gulp.src("./assets/pages/*.php")
    .pipe(php2html())
    .pipe(gulp.dest("./dist/pages"));
});

gulp.task('transform-html', function() {
  return gulp.src(["./dist/pages/*.html", "!./dist/pages/hi.html"])
    .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
    .pipe(rename(function (path) {
        path.extname = ".php";
      }))
    .pipe(gulp.dest("./dist/pages"));
});


gulp.task('image', function() {
    gulp.src('assets/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
});

// Clean output directory
gulp.task('clean', () => del(['dist']));

// Gulp task to minify all files
gulp.task('default', ['clean'], function () {
  runSequence(
    'styles'
    ,'scripts'
    ,'pages'
    ,'transform-html'
    ,'image'
  );
});