<!DOCTYPE html>
<html>

<?php

if($mode === 'debug')
    $img = 'assets';
else
    $img = 'dist';

?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Smart-Pyramid - смарт контракт с доходностью 1.23%">
    <meta name="keywords" content="smart-pyramid, smart pyramid, смарт пирамид, умная пирамида, eth pyramid, eth пирамида, 333eth , изиинвест, Easyinvest, ETH Pyramid, ETH hyip, hyip project, hyip">
    <title>
        <?php echo $title; ?>
    </title>
    <!-- Favicon-->
    <link href="./<?php echo $img; ?>/img/logo3.png" rel="icon" type="image/png">
    <!-- Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <!-- Smart-Pyramid CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
    
    <?php if($mode === 'debug') {  ?>
    
    <link type="text/css" href="./assets/css/style.css?time=<?php echo time(); ?>" rel="stylesheet">
    
    <?php } else {  ?>
    
    <link type="text/css" href="./dist/css/style.css" rel="stylesheet">
    
    <?php } ?>
    
    
</head>

<body>
    <header class="header-global landing">
        <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom backgroud-primary">
            <div class="container container-lg">
                <a href="/" class="navbar-brand mr-lg-5 d-inline-flex align-items-center"><img src="./<?php echo $img; ?>/img/logo3.png" class="dark-logo"><img src="./<?php echo $img; ?>/img/logo-color_.png" class="white-logo"><span style="margin-left: .7rem">Smart-Pyramid</span>
                </a>
                <button type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span class="navbar-toggler-icon"></span>
                </button>
                <div id="navbar_global" class="navbar-collapse collapse">
                    <div class="navbar-collapse-header">
                        <div class="row">
                            <div class="col-6 collapse-brand">
                                <a href="/"><img src="./<?php echo $img; ?>/img/logo3.png">
                                </a>
                            </div>
                            <div class="col-6 collapse-close">
                                <button type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler hamburger"><span></span><span></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <ul class="navbar-nav navbar-nav-hover align-items-lg-center ml-lg-auto">
                        <li class="nav-item dropdown"><a href="#" data-toggle="dropdown" role="button" class="nav-link"><span class="nav-link-inner--text">EN</span></a>
                            <div class="dropdown-menu">
                                <a href="/?lang=ru" class="dropdown-item">RU</a>
                                <a href="/?lang=en" class="dropdown-item">EN</a>
                                <a href="/?lang=es" class="dropdown-item">ES</a>
                                <a href="/?lang=fr" class="dropdown-item">FR</a>
                                <a href="/?lang=ar" class="dropdown-item">ARAB</a>
                                <a href="/?lang=id" class="dropdown-item">ind</a>
                                <a href="/?lang=ja" class="dropdown-item">JP</a>
                                <a href="/?lang=hi" class="dropdown-item">HI</a>
                                <a href="/?lang=pl" class="dropdown-item">PL</a>
                                <a href="/?lang=ml" class="dropdown-item">ML</a>
                                <a href="/?lang=vi" class="dropdown-item">VIET</a>
                                <a href="/?lang=zh" class="dropdown-item">CN</a>
                                <a href="/?lang=kaz" class="dropdown-item">KAZ</a>
                            </div>

                        </li>
                        <li class="nav-item dropdown"></li>
                        <li class="nav-item"><a onClick="gtag('event','Click1',{'event_category':'Button1','event_label':'Telegram1'});" href="https://t.me/Smart_Pyramid" class="nav-link nav-link-icon"><i class="fa fa-paper-plane"></i><span class="nav-link-inner--text d-lg-none">Telegram</span></a>
                        </li>
                    </ul>
                    <ul class="navbar-nav navbar-nav-hover align-items-lg-center ml-lg-auto">
                        <li class="nav-item dropdown"><a href="#hiw_" role="button" class="nav-link"><span class="nav-link-inner--text"><?php echo $text1 ?></span></a>
                        </li>
                        <li class="nav-item dropdown"><a href="#why_we_" role="button" class="nav-link"><span class="nav-link-inner--text"><?php echo $text2 ?></span></a>
                        </li>
                        <li class="nav-item dropdown"><a href="#about" role="button" class="nav-link"><span class="nav-link-inner--text"><?php echo $text3 ?></span></a>
                        </li>
                        <li class="nav-item dropdown"><a href="#investor_" role="button" class="nav-link"><span class="nav-link-inner--text"><?php echo $text4 ?></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main>
        <section class="section section-hero section-shaped p-xs-b-0">
            <div class="container py-md-4">
                <div class="row row-grid justify-content-between align-items-center">
                    <div class="col-lg-7 text-mobile-center mt-sm-4 text-center">
                        <h3 class="display-2 mt-4 mt-sm-0"><?php echo $text5 ?> 
              </h3>
                        <h4><?php echo $text6 ?><sup><a href="#" data-toggle="tooltip" title="<?php echo $text122 ?>" class="text-bolder">?</a></sup>
              </h4>
                        <h5><?php echo $text8 ?>
              </h5>
                        
                        <form id="addem" name="addem" action="" class="form-group mt-4 mb-2 eth-label col-10">
                            <input type="text" name="tb" value="10" autocomplete="on" class="form-control radius-right-non radius-left eth-label text-center top-calc">
                            <button onClick="gtag('event','click',{'event_category':'button1','event_label':'invets'});" type="button" data-toggle="modal" data-target="#modal-notification" class="btn btn-block btn-primary radius-left-non m-0">
                                <?php echo $text9 ?>
                            </button>
                            
                            <div id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true" class="modal fade">

                                <div role="document" class="modal-dialog modal-danger modal-dialog-centered modal-">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <h5><?php echo $text10 ?></h5>
                                                <!--<span class="message d-block">Скопировано!</span> -->
                                                <div class="form-group mt-4 mb-2 col-12">
                                                    <input id="copy5" type="text" value="0x3dA6d8F14dFcd576c46f9994D8Fd94883A8F3fe8" class="form-control radius-right-non radius-left col-8">
                                                    <button type="button" class="btn btn-block btn-primary radius-left-non">
                                                        <?php echo $copy ?>
                                                    </button>

                                                </div>
                                                <span class="data_text">
                                                   <?php echo $data_text ?><font color="#8a2be2" class="value"></font><br>
                                                </span>
                                                <h6><?php echo $text12 ?></h6>
                                                <h6> <?php echo $text13 ?></h6>
                                                <h5 class="text-warning"><?php echo $text14 ?></h5>
                                                <button type="button" data-dismiss="modal" style="margin: 0 auto" class="btn btn-white ml-auto mt-4">
                                                    <?php echo $text15 ?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--<h6> <?php echo $text12 ?></h6>-->
                        <h5 class="mt-4 text-primary">Калькулятор доходности:</h5>
                        <input type="range" class="income-calc" min="0" max="365" value="0" step="1">
                        <div class="col-11 d-inline-flex">
                            <h6 class="text-uppercase font-weight-bold text-left col-4">0</h6>
                            <h6 class="text-uppercase font-weight-bold text-center col-4 income-calc-val"><span class="number">0</span> <?php echo $text19_1; ?></h6>
                            <h6 class="text-uppercase font-weight-bold text-right col-4">365 <?php echo $text19_1; ?></h6>
                          </div>
                        <div class="col-12 col-sm-10 offset-sm-1">
                            <div class="row row-grid mt-4">
                                <div class="col-12 offset-sm-0 col-sm-6 mt-sm-0">
                                    <div class="border-0 card card-body shadow">
                                        <div class="text-center">
                                            <h5 class="text-primary"><?php echo $text17 ?></h5>
                                            <h5 id="update">0.0000014</h5><span class="label">ETH</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 offset-sm-0 col-sm-6 mt-sm-0">
                                    <div class="border-0 card card-body shadow">
                                        <div class="text-center">
                                            <h5 class="text-primary"><?php echo $text18 ?></h5>
                                            <h5 id="update1">4.59</h5><span class="label">ETH</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 order-md-2 text-center screen_first">
                        <span id="space">
                            <div class="elogo">
                                   <div class="trif u1">
                                     <img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1539807262/triangle.png" alt="">
                                     
                                   </div>
                                   <div class="trif u2">
                                     <img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1539807262/triangle.png" alt="">
                                     
                                   </div>
                                   <div class="trif u3">
                                     <img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1539807262/triangle.png" alt="">
                                     
                                   </div>
                                   <div class="trif u4">
                                     <img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1539807262/triangle.png" alt="">
                                   </div>
                                   <div class="ct"></div>
                                   <div class="ct1">
                                     
                                   </div>
                                   <div class="trif l1">
                                     <img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1539807262/triangle.png" alt="">
                                     
                                   </div>
                        
                                   <div class="trif l2">
                                     <img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1539807262/triangle.png" alt="">
                                   </div>
                                   <div class="trif l3">
                                     <img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1539807262/triangle.png" alt="">
                                   </div>
                        
                                   <div class="trif l4">
                                     <img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1539807262/triangle.png" alt="">
                                   </div>
                            </div>
                        </span>
                        <img src="./<?php echo $img; ?>/img/4eth.png" class="position-absolute float">
                        <img src="./<?php echo $img; ?>/img/1eth.png">
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="container mt-5">
                    <div class="row row-grid mt-5">
                        <div class="col-lg-3 col-10 offset-1 offset-sm-0 col-sm-6 mt-sm-0">
                            <div class="border-0">
                                <div class="card-body text-center single_counter day-counter-statistic"><img src="./<?php echo $img; ?>/img/1.png" alt="" width="80" class="img-fluid mb-3">
                                    <h6 class="text-primary"><?php echo $work ?></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-10 offset-1 offset-sm-0 col-sm-6 mt-sm-0">
                            <div class="border-0">
                                <div class="card-body text-center single_counter invest-counter-statistic"><img src="./<?php echo $img; ?>/img/2.png" alt="" width="80" class="img-fluid mb-3">
                                    <h6 class="text-primary"><?php echo $invest ?></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-10 offset-1 offset-sm-0 col-sm-6 mt-sm-0">
                            <div class="border-0">
                                <div class="card-body text-center single_counter withdraw-counter-statistic"><img src="./<?php echo $img; ?>/img/3.png" alt="" width="80" class="img-fluid mb-3">
                                    <h6 class="text-primary"><?php echo $widthdrow ?></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-10 offset-1 offset-sm-0 col-sm-6 mt-sm-0">
                            <div class="border-0">
                                <div class="card-body text-center single_counter people-counter-statistic"><img src="./<?php echo $img; ?>/img/4.png" alt="" width="80" class="img-fluid mb-3">
                                    <h6 class="text-primary"><?php echo $peoples ?></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="hiw_" class="section section-lg p-xs-b-0">
            <div class="container">
                <div class="row justify-content-center mb-4">
                    <div class="col-lg-8 text-center mb-4">
                        <h2 id="hiw" class="display-3"><?php echo $text23?></h2>
                    </div>
                </div>
                <div class="nav-wrapper">
                    <ul id="tabs-icons-text" role="tablist" class="nav nav-pills nav-fill flex-column flex-md-row">
                        <li class="nav-item col-lg-3 col-12 col-sm-8 offset-sm-2 offset-lg-0">
                            <a id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true" class="nav-link mb-sm-3 mb-md-0 active card shadow border-0 step step-1">
                                <div class="position-absolute shadow-t icon icon-1"></div>
                                <div class="card-body text-center">
                                    <h6 class="mt-3"><?php echo $text24?> </h6>
                                    <button type="button" class="btn mt-2 btn-primary">
                                        <?php echo $moreinfo ?>
                                    </button>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item col-lg-3 col-12 col-sm-8 offset-sm-2 offset-lg-0">
                            <a id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false" class="nav-link mb-sm-3 mb-md-0 card shadow border-0 step step-2">
                                <div class="position-absolute shadow-t icon icon-2"></div>
                                <div class="card-body text-center">
                                    <h6 class="mt-3"><?php echo $text25?></h6>
                                    <button type="button" class="btn mt-2 btn-primary">
                                        <?php echo $moreinfo ?>
                                    </button>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item col-lg-3 col-12 col-sm-8 offset-sm-2 offset-lg-0">
                            <a id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false" class="nav-link mb-sm-3 mb-md-0 card shadow border-0 step step-3">
                                <div class="position-absolute shadow-t icon icon-3"></div>
                                <div class="card-body text-center">
                                    <h6 class="mt-3"><?php echo $text26?></h6>
                                    <button type="button" class="btn mt-2 btn-primary">
                                        <?php echo $moreinfo ?>
                                    </button>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item col-lg-3 col-12 col-sm-8 offset-sm-2 offset-lg-0">
                            <a id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false" class="nav-link mb-sm-3 mb-md-0 card shadow border-0 step step-4">
                                <div class="position-absolute shadow-t icon icon-4"></div>
                                <div class="card-body text-center">
                                    <h6 class="mt-3"><?php echo $text27?></h6>
                                    <button type="button" class="btn mt-2 btn-primary">
                                        <?php echo $moreinfo ?>
                                    </button>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div id="myTabContent" class="tab-content">
                        <div id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab" class="tab-pane fade show active">
                            <div class="row row-grid justify-content-center">
                                <div class="col-lg-10 text-center mt-4">
                                    <p class="lead">
                                        <?php echo $text28?>
                                        <div class="nav-wrapper">
                                            <ul id="tabs-text" role="tablist" class="nav nav-pills nav-fill flex-md-row row justify-content-center align-items-center">
                                            </ul>
                                        </div>
                                        <div id="myTabContent" class="tab-content">
                                            <div id="tabs-step-1" role="tabpanel" aria-labelledby="tabs-step-1-tab" class="tab-pane fade show active">
                                                <ul class="list-unstyled col-12 offset-0 col-sm-10 offset-sm-1 text-left mt-5">
                                                    <li class="py-2">
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h6 class="mb-0"><?php echo $text29?></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="py-2">
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h6 class="mb-0"><?php echo $text30?></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="py-2">
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h6 class="mb-0"><?php echo $text31?></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="tabs-step-2" role="tabpanel" aria-labelledby="tabs-step-2-tab" class="tab-pane fade">
                                                <ul class="list-unstyled col-12 offset-0 col-sm-10 offset-sm-1 text-left mt-5">
                                                    <li class="py-2">
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h6 class="mb-0"><?php echo $text32?></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="py-2">
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h6 class="mb-0"><?php echo $text33?></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="py-2">
                                                        <div class="d-flex align-items-center">
                                                            <div>
                                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h6 class="mb-0"><?php echo $text34?></h6>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab" class="tab-pane fade">
                            <div class="row row-grid justify-content-center">
                                <div class="col-lg-10 text-center mt-4">
                                    <ul class="list-unstyled col-12 offset-0 col-sm-10 offset-sm-1 text-left mt-5">
                                        <li class="py-2">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h6 class="mb-0"><?php echo $text35?></h6>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="py-2">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h6 class="mb-0"><?php echo $text36?></h6>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="py-2">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h6 class="mb-0"><?php echo $text37?></h6>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="py-2">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h6 class="mb-0"><?php echo $text38?></h6>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab" class="tab-pane fade">
                            <div class="row row-grid justify-content-center">
                                <div class="col-lg-10 text-center mt-4">
                                    <div class="nav-wrapper">
                                        <ul id="tabs-text2" role="tablist" class="nav nav-pills nav-fill flex-md-row row justify-content-center align-items-center">
                                        </ul>
                                    </div>
                                    <div id="myTabContent" class="tab-content">
                                        <div id="tabs-step-5" role="tabpanel" aria-labelledby="tabs-step-5-tab" class="tab-pane fade show active">
                                            <h5 class="mt-4"><?php echo $text39?> </h5>
                                            <form class="form-group mt-4 mb-2 eth-label col-lg-8">
                                                <input type="text" name="tb" value="10" autocomplete="on" class="form-control radius-right-non radius-left eth-label text-center">
                                                <button onClick="gtag('event','Click4',{'event_category':'Button4','event_label':'Invest2'});" type="button" data-toggle="modal" data-target="#modal-notification" class="btn btn-block btn-primary radius-left-non m-0">
                                                    <?php echo $text9?>
                                                </button>
                                            </form>
                                            <h6> <?php echo $text40?></h6>
                                            <h6> <?php echo $text41?></h6>
                                            <h6 class="mt-3"><?php echo $text42?>
<?php echo $text42_2?>
</h6>
                                        </div>
                                        <div id="tabs-step-6" role="tabpanel" aria-labelledby="tabs-step-6-tab" class="tab-pane fade">
                                            <h5 class="mt-4"><?php echo $text43?></h5>
                                            <span class="message d-block"><?php echo $text58?></span>
                                            <div class="form-group mt-4 mb-2 col-lg-8">
                                                <input id="copy2" type="text" value="0x3dA6d8F14dFcd576c46f9994D8Fd94883A8F3fe8" readonly class="form-control radius-right-non radius-left col-8">
                                                <button type="button" class="btn btn-block btn-primary radius-left-non">
                                                    <?php echo $copy?>
                                                </button>
                                            </div>
                                            <h6><?php echo $text44?> </h6>
                                            <h6><?php echo $text45?> </h6>
                                            <h5 class="text-warning"><?php echo $text14?></h5>
                                            <h6 class="mt-3"><?php echo $text42?>
</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab" class="tab-pane fade">
                            <div class="row row-grid justify-content-center">
                                <div class="col-lg-10 text-center mt-4">
                                    <h3 class="display-4 text-center"><?php echo $text46?>
                    </h3>
                                    <p class="lead">
                                        <?php echo $text47?>
                                        <div class="nav-wrapper">
                                            <ul id="tabs-text" role="tablist" class="nav nav-pills nav-fill flex-md-row row justify-content-center align-items-center">
                                                <li class="col-lg-4 col-6">
                                                    <a id="tabs-step-7-tab" data-toggle="tab" href="#tabs-step-7" role="tab" aria-controls="tabs-step-7" aria-selected="true" class="mb-sm-3 mb-md-0 active"> <img src="https://en.bitcoinwiki.org/upload/en/images/thumb/e/eb/Metamask.png/400px-Metamask.png" class="img-fluid">
                                                    </a>
                                                </li>
                                                <li class="col-lg-4 col-6">
                                                    <a id="tabs-step-8-tab" data-toggle="tab" href="#tabs-step-8" role="tab" aria-controls="tabs-step-8" aria-selected="false" class="mb-sm-3 mb-md-0"> <img src="https://www.myetherwallet.com/images/myetherwallet-logo.png" class="img-fluid">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="myTabContent" class="tab-content">
                                            <div id="tabs-step-7" role="tabpanel" aria-labelledby="tabs-step-7-tab" class="tab-pane fade show active">
                                                <h5 class="mt-4"><?php echo $text49?></h5>
                                                <form class="form-group mt-4 mb-2 col-lg-8">
                                                    <button type="button" id="getWithdrawal" class="btn btn-block btn-primary m-0">Получить выплату</button>
                                                </form>
                                                <h6> <?php echo $text51?></h6>
                                                <h6> <?php echo $text13?></h6>
                                            </div>
                                            <div id="tabs-step-8" role="tabpanel" aria-labelledby="tabs-step-8-tab" class="tab-pane fade">
                                                <h5 class="mt-4"><?php echo $text53?></h5>
                                                <span class="message d-block"><?php echo $text58?></span>
                                                <div class="form-group mt-4 mb-2 col-8">
                                                    <input id="copy3" type="text" , value="0x3dA6d8F14dFcd576c46f9994D8Fd94883A8F3fe8" readonly class="form-control radius-right-non radius-left col-8">
                                                    <button type="button" class="btn btn-block btn-primary radius-left-non">
                                                        <?php echo $copy?>
                                                    </button>
                                                </div>
                                                <h6> <?php echo $text13?></h6>
                                            </div>
                                            <p class="text-small">
                                                <?php echo $text48?>
                                                <br>
                                                <?php echo $text121?>
                                            </p>
                                        </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="hiw-m" class="section section-lg p-xs-b-0">
            <div class="container">
                <div class="row justify-content-center mb-4">
                    <div class="col-lg-8 text-center mb-4">
                        <h2 class="display-3"><?php echo $text55?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mt-5">
                        <div class="topic">
                            <div class="open">
                                <div class="col-lg-3 col-12 col-sm-8 offset-sm-2 offset-lg-0">
                                    <div class="card shadow border-0 step step-1">
                                        <div class="position-absolute shadow-t icon icon-1"></div>
                                        <div class="card-body text-center">
                                            <h6 class="mt-3"><?php echo $text56?> </h6>
                                            <button type="button" class="btn mt-2 btn-primary">
                                                <?php echo $moreinfo?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="answer">
                                <p class="lead">
                                    <?php echo $text57?> </p>
                                <div class="nav-wrapper">
                                    <ul id="tabs-text" role="tablist" class="nav nav-pills nav-fill flex-md-row row justify-content-center align-items-center">
                                        <li class="col-lg-4 col-4">
                                            <a id="tabs-step-9-tab" data-toggle="tab" href="#tabs-step-9" role="tab" aria-controls="tabs-step-9" aria-selected="true" class="mb-sm-3 mb-md-0 active"> <img src="https://en.bitcoinwiki.org/upload/en/images/thumb/e/eb/Metamask.png/400px-Metamask.png" class="img-fluid">
                                            </a>
                                        </li>
                                        <li class="col-lg-4 col-4">
                                            <a id="tabs-step-10-tab" data-toggle="tab" href="#tabs-step-10" role="tab" aria-controls="tabs-step-10" aria-selected="false" class="mb-sm-3 mb-md-0"> <img src="https://www.myetherwallet.com/images/myetherwallet-logo.png" class="img-fluid">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div id="myTabContent" class="tab-content">
                                    <div id="tabs-step-9" role="tabpanel" aria-labelledby="tabs-step-9-tab" class="tab-pane fade show active">
                                        <ul class="list-unstyled col-12 offset-0 col-sm-10 offset-sm-1 text-left mt-5">
                                            <li class="py-2">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <h6 class="mb-0"><?php echo $text29?></h6>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="py-2">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <h6 class="mb-0"><?php echo $text30?></h6>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="py-2">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <h6 class="mb-0"><?php echo $text31?></h6>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="tabs-step-10" role="tabpanel" aria-labelledby="tabs-step-10-tab" class="tab-pane fade">
                                        <ul class="list-unstyled col-12 offset-0 col-sm-10 offset-sm-1 text-left mt-5">
                                            <li class="py-2">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <h6 class="mb-0"><?php echo $text32?></h6>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="py-2">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <h6 class="mb-0"><?php echo $text33?></h6>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="py-2">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <h6 class="mb-0"><?php echo $text34?></h6>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="topic">
                            <div class="open">
                                <div class="col-lg-3 col-12 col-sm-8 offset-sm-2 offset-lg-0">
                                    <div class="card shadow border-0 step step-2">
                                        <div class="position-absolute shadow-t icon icon-2"></div>
                                        <div class="card-body text-center">
                                            <h6 class="mt-3"><?php echo $text25;?> </h6>
                                            <button type="button" class="btn mt-2 btn-primary">
                                                <?php echo $moreinfo ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="answer">
                                <ul class="list-unstyled col-12 offset-0 col-sm-10 offset-sm-1 text-left mt-5">
                                    <li class="py-2">
                                        <div class="d-flex align-items-center">
                                            <div>
                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                </div>
                                            </div>
                                            <div>
                                                <h6 class="mb-0"><?php echo $text35;?></h6>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="py-2">
                                        <div class="d-flex align-items-center">
                                            <div>
                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                </div>
                                            </div>
                                            <div>
                                                <h6 class="mb-0"><?php echo $text36;?></h6>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="py-2">
                                        <div class="d-flex align-items-center">
                                            <div>
                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                </div>
                                            </div>
                                            <div>
                                                <h6 class="mb-0"><?php echo $text37;?></h6>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="py-2">
                                        <div class="d-flex align-items-center">
                                            <div>
                                                <div class="badge badge-circle badge-primary mr-3"><i class="fa fa-check"></i>
                                                </div>
                                            </div>
                                            <div>
                                                <h6 class="mb-0"><?php echo $text38;?></h6>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="topic">
                            <div class="open">
                                <div class="col-lg-3 col-12 col-sm-8 offset-sm-2 offset-lg-0">
                                    <div class="card shadow border-0 step step-3">
                                        <div class="position-absolute shadow-t icon icon-3"></div>
                                        <div class="card-body text-center">
                                            <h6 class="mt-3"> <h6 class="mb-0"><?php echo $text26;?></h6> </h6>
                                            <button type="button" class="btn mt-2 btn-primary">
                                                <?php echo $moreinfo; ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="answer">
                                <div class="nav-wrapper">

                                </div>
                                <div id="myTabContent" class="tab-content">
                                    <div id="tabs-step-11" role="tabpanel" aria-labelledby="tabs-step-11-tab" class="tab-pane fade show active">
                                        <h5 class="mt-4"><?php echo $text39;?> </h5>
                                        <form class="form-group mt-4 mb-2 eth-label col-lg-8">
                                            <input type="text" name="tb" value="10" autocomplete="on" class="form-control radius-right-non radius-left eth-label text-center">
                                            <button type="button" data-toggle="modal" data-target="#modal-notification" class="btn btn-block btn-primary radius-left-non m-0">
                                                <?php echo $text9;?>
                                            </button>
                                        </form>

                                        <h6> <?php echo $text12?></h6>
                                        <h6> <?php echo $text13?></h6>
                                        <h6 class="mt-3"><?php echo $text42?></h6>
                                    </div>
                                    <div id="tabs-step-12" role="tabpanel" aria-labelledby="tabs-step-12-tab" class="tab-pane fade">
                                        <h5 class="mt-4"><?php echo $text43?></h5><span class="message d-block"><?php echo $text58?></span>
                                        <div class="form-group mb-2 col-lg-8">
                                            <input id="copy5" type="text" , value="0x3dA6d8F14dFcd576c46f9994D8Fd94883A8F3fe8" readonly class="form-control radius-right-non radius-left col-8 text">
                                            <button type="button" class="btn btn-block btn-primary radius-left-non button">
                                                <?php echo $copy?>
                                            </button>
                                        </div>
                                        <span class="data_text">
                                            <?php echo $data_text ?><font color="#8a2be2" class="value"></font><br>
                                        </span>
                                        <h6> <?php echo $text12?></h6>
                                        <h6> <?php echo $text13?></h6>
                                        <h5 class="text-warning"><?php echo $text14?> </h5>
                                        <h6 class="mt-3"><?php echo $text42?></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="topic">
                            <div class="open">
                                <div class="col-lg-3 col-12 col-sm-8 offset-sm-2 offset-lg-0">
                                    <div class="card shadow border-0 step step-4">
                                        <div class="position-absolute shadow-t icon icon-4"></div>
                                        <div class="card-body text-center">
                                            <h6 class="mt-3"><?php echo $text27?></h6>
                                            <button type="button" class="btn mt-2 btn-primary">
                                                <?php echo $moreinfo ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="answer">
                                <p class="lead">
                                    <?php echo $text47?>
                                    <p class="lead">
                                        <?php echo $text48?>
                                        <div class="nav-wrapper">
                                            <ul id="tabs-text" role="tablist" class="nav nav-pills nav-fill flex-md-row row justify-content-center align-items-center">
                                                <li class="col-lg-4 col-4">
                                                    <a id="tabs-step-13-tab" data-toggle="tab" href="#tabs-step-13" role="tab" aria-controls="tabs-step-13" aria-selected="true" class="mb-sm-3 mb-md-0 active"> <img src="https://en.bitcoinwiki.org/upload/en/images/thumb/e/eb/Metamask.png/400px-Metamask.png" class="img-fluid">
                                                    </a>
                                                </li>
                                                <li class="col-lg-4 col-4">
                                                    <a id="tabs-step-14-tab" data-toggle="tab" href="#tabs-step-14" role="tab" aria-controls="tabs-step-14" aria-selected="false" class="mb-sm-3 mb-md-0"> <img src="https://www.myetherwallet.com/images/myetherwallet-logo.png" class="img-fluid">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="myTabContent" class="tab-content">
                                            <div id="tabs-step-13" role="tabpanel" aria-labelledby="tabs-step-13-tab" class="tab-pane fade show active">
                                                <h5 class="mt-4"><?php echo $text49?></h5>
                                                <form class="form-group mt-4 mb-2 col-lg-8">
                                                    <button type="button" class="btn btn-block btn-primary m-0">
                                                        <?php echo $text50?>
                                                    </button>
                                                </form>
                                                <h6> <?php echo $text51?></h6>
                                                <h6> <?php echo $text13?></h6>
                                            </div>
                                            <div id="tabs-step-14" role="tabpanel" aria-labelledby="tabs-step-14-tab" class="tab-pane fade">
                                                <h5 class="mt-4"><?php echo $text53?></h5><span class="message d-block"><?php echo $text58?></span>
                                                <div class="form-group mb-2 col-8">
                                                    <input id="copy4" type="text" , value="0x3dA6d8F14dFcd576c46f9994D8Fd94883A8F3fe8" readonly class="form-control radius-right-non radius-left col-8 text">
                                                    <button type="button" class="btn btn-block btn-primary radius-left-non button">
                                                        <?php echo $copy?>
                                                    </button>
                                                </div>
                                                <h6> <?php echo $text13?></h6>
                                            </div>
                                        </div>
                                    </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="why_we_" class="section section-lg p-xs-b-0">
            <div class="container">
                <div class="row row-grid justify-content-center">
                    <div class="col-lg-8 text-center mb-4">
                        <h2 id="why_we" class="display-3"><?php echo $text59?>
                
              </h2>
                    </div>
                    <div class="col-lg-6 order-lg-1 mt-5">
                        <div class="text-center px-3">
                            <h4 class="display-3"><?php echo $text60?></h4>
                        </div>
                        <div class="px-3">
                            <div class="d-flex align-items-center"><img src="./<?php echo $img; ?>/img/1-icon.png" alt="" width="70">
                                <p class="pl-5">
                                    <?php echo $text61?>
                                </p>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="d-flex align-items-center"><img src="./<?php echo $img; ?>/img/2-icon.png" alt="" width="70">
                                <p class="pl-5">
                                    <?php echo $text62?>
                                </p>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="d-flex align-items-center"><img src="./<?php echo $img; ?>/img/3-icon.png" alt="" width="70">
                                <p class="pl-5">
                                    <?php echo $text63?>
                                </p>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="d-flex align-items-center"><img src="./<?php echo $img; ?>/img/4-icon.png" alt="" width="70">
                                <p class="pl-5">
                                    <?php echo $text64?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 order-lg-1 mt-5">
                        <div class="text-center px-3">
                            <h4 class="display-3"><?php echo $title?></h4>
                        </div>
                        <div class="px-3">
                            <div class="d-flex align-items-center"><img src="./<?php echo $img; ?>/img/5-icon.png" alt="" width="70">
                                <p class="pl-5">
                                    <?php echo $text65?>
                                </p>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="d-flex align-items-center"><img src="./<?php echo $img; ?>/img/6-icon.png" alt="" width="70">
                                <p class="pl-5">
                                    <?php echo $text66?>
                                    <button onClick="gtag('event','Click5',{'event_category':'Button5','event_label':'Itogi'});" type="button" data-toggle="modal" data-target="#modal-default" class="btn btn-block btn-white mt-3">
                                        <?php echo $text67?>
                                    </button>
                                </p>
                                <div id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true" class="modal fade">
                                    <div role="document" class="modal-dialog modal- modal-dialog-centered modal-">
                                        <div class="modal-content">
                                            <div class="card shadow shadow-lg--hover">
                                                <div class="card-body">
                                                    Итоги на русском: <a href="https://smart-pyramid.io/Audit.pdf" target="blank">Audit</a>
                                                    <br> English: <a href="https://smart-pyramid.io/AuditEN.pdf" target="blank">AuditEN</a>
                                                    <br>
                                                    <!--     <iframe width="100%" height="315" src="https://www.youtube.com/embed/a-Azm3nEuUI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe> -->
                                                    <? if($lang=='ru' ) { echo '<iframe width="100%" height="315" src="https://www.youtube.com/embed/mgTZdtqdVnw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'; } else{ echo '<iframe width="100%" height="315" src="https://www.youtube.com/embed/iuqoT_tTTaM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'; } ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="d-flex align-items-center"><img src="./<?php echo $img; ?>/img/7-icon.png" alt="" width="70">
                                <p class="pl-5">
                                    <?php echo $text68?>
                                </p>
                            </div>
                        </div>
                        <div class="px-3">
                            <div class="d-flex align-items-center"><img src="./<?php echo $img; ?>/img/8-icon.png" alt="" width="70">
                                <p class="pl-5">
                                    <?php echo $text69?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="about" class="section section-lg p-xs-b-0">
            <div class="container">
                <div class="row row-grid justify-content-center">
                    <div class="col-lg-8 text-center mb-4">
                        <h2 id="distrib" class="display-3"><?php echo $text70?></h2>
                    </div>
                    <div class="position-relative graph text-center"><img src="./<?php echo $img; ?>/img/diagram-mmm_.png" class="img-fluid mt-5 big"><img src="./<?php echo $img; ?>/img/diagram-mmm__300.png" class="img-fluid mt-5 small">
                        <div class="row row-grid justify-content-center">
                            <h5 class="distrib distrib_1"><?php echo $text71?>
                </h5>
                            <h5 class="distrib distrib_2"><?php echo $text72?>
                </h5>
                            <h5 class="distrib distrib_3"><?php echo $text73?>
                </h5>
                            <h5 class="distrib distrib_4"><?php echo $text74?>
                </h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="investor_" class="section section-lg p-xs-b-0">
            <div class="container">
                <div class="row row-grid justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h2 id="investor" class="display-3"><?php echo $text75?></h2>
                        <p class="lead">
                            <?php echo $text76?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="border-0">
                            <div class="card-header bg-transp">
                                <div class="text-center mb-3 display-3">
                                    <h5 class="display-3 text-primary"><?php echo $text77?></h5>
                                </div>
                                <div class="container countdown">
                                    <ul class="text-primary">
                                        <li><span id="days" class="text-dark"></span>
                                            <?php echo $text78?>
                                        </li>
                                        <li><span id="hours" class="text-dark"></span>
                                            <?php echo $text79?>
                                        </li>
                                        <li><span id="minutes" class="text-dark"></span>
                                            <?php echo $text80?>
                                        </li>
                                        <li><span id="seconds" class="text-dark"></span>
                                            <?php echo $text81?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card shadow shadow-lg--hover">
                            <div class="card-body">
                                <h3 class="display-3 text-center"><?php echo $text82?></h3>
                                <ul class="current-winner list-unstyled col-12 mt-4">
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card shadow shadow-lg--hover mt-5">
                            <div class="card-body">
                                <h3 class="display-3 text-center"><?php echo $text83?></h3>
                                <ul class="past-winner list-unstyled hide-content mt-4">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5 justify-content-center">
                    <div class="col-lg-12 text-center">
                        <p class="text-small">
                            <?php echo $text84?>
                        </p>
                        <p class="text-small">
                            <?php echo $text85?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-lg p-xs-b-0">
            <div class="container">
                <div class="row row-grid justify-content-center mb-5">
                    <div class="col-lg-8 text-center">
                        <h2 id="program" class="display-3"><?php echo $text86?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-mobile-center mt-5 mt-lg-0 mb-4">
                            <h3 class="display-4 text-center"><?php echo $text87?>
                </h3>
                        </div>
                    </div>
                    <div class="col-lg-6 order-lg-2 text-center mt-5">
                        <div class="card shadow shadow-lg--hover">
                            <div class="card-body">
                                <h3 class="display-3"><?php echo $text88?></h3>
                                <p class="lead">
                                    <?php echo $text89?>
                                </p>
                                <div class="form-group mb-2">
                                    <input type="text" id="referralAddress" placeholder="Введите адресс" class="form-control radius-right-non radius-left">
                                    <button data-toggle="collapse" id="referralAddressButton" data-target="#gen" class="btn btn-block btn-primary radius-left-non">
                                        <?php echo $text90?>
                                    </button>
                                </div>
                                <div id="gen" class="collapse">
                                    <p class="lead">
                                        <?php echo $text91?>
                                    </p>
                                    <div class="form-group mb-2 col-12 p-0">
                                        <input id="copy1" type="text" readonly class="referralLink form-control radius-right-non radius-left col-sm-8 col-12">
                                        <button type="button" class="btn btn-block btn-primary radius-left-non col-sm-4 col-12">
                                            <?php echo $copy?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" data-toggle="modal" data-target="#modal-form" class="btn btn-block btn-white mt-5 col-12 col-sm-8 offset-sm-2">
                            <?php echo $text92?>
                        </button>
                        <div id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true" class="modal fade">
                            <div role="document" class="modal-dialog modal- modal-dialog-centered modal-sm">
                                <div class="modal-content">
                                    <div class="modal-body p-0">
                                        <div class="card bg-secondary shadow border-0">
                                            <div class="card-body px-lg-5 py-lg-5">
                                                <h5 class="text-left">Banner GIF (200x300)</h5><img src="https://smart-pyramid.io/banners_<?=$lang;?>/200x300.gif?v=1" class="img-fluid mt-2 mb-2">
                                                <h5 class="text-left"><?php echo $text93?></h5>
                                                <div class="form-group mb-2 col-12">
                                                    <a href="https://smart-pyramid.io/banners_<?=$lang;?>/200x300.gif" target="blank">https://smart-pyramid.io/banners_<?=$lang;?>/200x300.gif</a>
                                                </div>
                                                <hr>
                                                <h5 class="text-left">Banner GIF (729x90)</h5><img src="https://smart-pyramid.io/banners_<?=$lang;?>/728x90.gif?v=1" class="img-fluid mt-2 mb-2">
                                                <h5 class="text-left"><?php echo $text93?></h5>
                                                <div class="form-group mb-2 col-12">
                                                    <a href="https://smart-pyramid.io/banners_<?=$lang;?>/728x90.gif" target="blank"> https://smart-pyramid.io/banners_<?=$lang;?>/728x90.gif</a>
                                                </div>
                                                <hr>
                                                <h5 class="text-left">Banner GIF (468x60)</h5><img src="https://smart-pyramid.io/banners_<?=$lang;?>/468x60.gif?v=1" class="img-fluid mt-2 mb-2">
                                                <h5 class="text-left"><?php echo $text93?></h5>
                                                <div class="form-group mb-2 col-12">
                                                    <a href="https://smart-pyramid.io/banners_<?=$lang;?>/468x60.gif" target="blank"> https://smart-pyramid.io/banners_<?=$lang;?>/468x60.gif</a>
                                                </div>
                                                <hr>


                                                <h5 class="text-left">Banner GIF (240x400)</h5><img src="https://smart-pyramid.io/banners_<?=$lang;?>/240x400.gif?v=1" class="img-fluid mt-2 mb-2">
                                                <h5 class="text-left"><?php echo $text93?></h5>
                                                <div class="form-group mb-2 col-12">
                                                    <a href="https://smart-pyramid.io/banners_<?=$lang;?>/240x400.gif" target="blank"> https://smart-pyramid.io/banners_<?=$lang;?>/240x400.gif</a>
                                                </div>
                                                <hr>
                                                <h5 class="text-left">Banner GIF (100x100)</h5><img src="https://smart-pyramid.io/banners_<?=$lang;?>/100x100.gif?v=1" class="img-fluid mt-2 mb-2">
                                                <h5 class="text-left"><?php echo $text93?></h5>
                                                <div class="form-group mb-2 col-12">
                                                    <a href="https://smart-pyramid.io/banners_<?=$lang;?>/100x100.gif" target="blank"> https://smart-pyramid.io/banners_<?=$lang;?>/100x100.gif</a>
                                                </div>
                                                <hr>
                                                <h5 class="text-left">Banner GIF (88x31)</h5><img src="https://smart-pyramid.io/banners_<?=$lang;?>/88x31.gif?v=1" class="img-fluid mt-2 mb-2">
                                                <h5 class="text-left"><?php echo $text93?></h5>
                                                <div class="form-group mb-2 col-12">
                                                    <a href="https://smart-pyramid.io/banners_<?=$lang;?>/88x31.gif" target="blank"> https://smart-pyramid.io/banners_<?=$lang;?>/88x31.gif</a>
                                                </div>
                                                <hr>

                                                <button type="button" data-dismiss="modal" style="margin: 0 auto" class="btn btn-white ml-auto mt-4">
                                                    <?php echo $text15?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 order-lg-1">
                        <div class="text-mobile-center mt-5 mt-lg-0">
                            <p class="lead">
                                <?php echo $text94?>
                            </p>
                            <p class="lead">
                                <?php echo $text95?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-lg p-xs-b-0">
            <div class="container">
                <div class="row row-grid justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h2 id="account" class="display-3"><?php echo $text96?></h2>
                        <p class="lead">
                            <?php echo $text97?>
                        </p>
                        <div class="d-flex col-12 mt-4">
                            <div class="form-group mt-4 mb-2 col-12">
                                <input type="text" id="userInfoAddressInput" placeholder="<?php echo $text97_1?>" class="form-control radius-right-non radius-left">
                                <button id="showUserInfo" data-toggle="collapse" data-target="#panel" class="btn btn-block btn-primary radius-left-non">
                                    <?php echo $text98?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel" class="collapse">
                    <div class="row row-grid justify-content-center mt-5">
                        <div class="col-md-4">
                            <h3 class="text-center mb-4"><?php echo $text99?></h3>
                            <div class="table-scroll shadow-lg card">
                                <table class="userInvestment rwd-table">
                                    <thead>
                                        <tr>
                                            <th class="text-primary">
                                                <?php echo $text102?>
                                            </th>
                                            <th class="text-primary"><img src="./<?php echo $img; ?>/img/eth.png" alt="" width="15" class="img-fluid">
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3 class="text-center mb-4"><?php echo $text101?></h3>
                            <div class="table-scroll shadow-lg card">
                                <table class="userIncome rwd-table">
                                    <thead>
                                        <tr>
                                            <th class="text-primary">
                                                <?php echo $text102?>
                                            </th>
                                            <th class="text-primary">
                                                <?php echo $text103?>
                                            </th>
                                            <th class="text-primary"><img src="./<?php echo $img; ?>/img/eth.png" alt="" width="15" class="img-fluid">
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3 class="text-center mb-4"><?php echo $text104?></h3>
                            <div class="table-scroll shadow-lg card">
                                <table class="userReferrals rwd-table">
                                    <thead>
                                        <tr>
                                            <th class="text-primary">
                                                <?php echo $text105?>
                                            </th>
                                            <th class="text-primary"><img src="./<?php echo $img; ?>/img/eth.png" alt="" width="15" class="img-fluid">
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot></tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-lg p-xs-b-0">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center mb-4">
                        <h2 id="statistics" class="display-3"><?php echo $text106?></h2>
                    </div>
                </div>
                <div class="nav-wrapper">
                    <ul id="tabs-icons-text" role="tablist" class="nav nav-pills nav-fill flex-column flex-md-row align-items-center">
                        <li class="nav-item col-lg-3 col-10 offset-sm-0 col-sm-6 offset-lg-3 p-0">
                            <a id="tabs-icons-text-5-tab" data-toggle="tab" href="#tabs-icons-text-5" role="tab" aria-controls="tabs-icons-text-5" aria-selected="true" class="nav-link mb-sm-3 mb-md-0 active shadow-t border-0 radius-right-non radius-left radius-btn">
                                <h5 class="m-0"><?php echo $text107?></h5>
                            </a>
                        </li>
                        <li class="nav-item col-lg-3 col-10 offset-sm-0 col-sm-6 p-0">
                            <a id="tabs-icons-text-6-tab" data-toggle="tab" href="#tabs-icons-text-6" role="tab" aria-controls="tabs-icons-text-6" aria-selected="false" class="nav-link mb-sm-3 mb-md-0 shadow-t border-0 radius-left-non radius-right radius-btn">
                                <h5 class="m-0"><?php echo $text108?></h5>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="mt-4">
                    <div id="myTabContent" class="tab-content">
                        <div id="tabs-icons-text-5" role="tabpanel" aria-labelledby="tabs-icons-text-5-tab" class="tab-pane fade show active">
                            <div class="row row-grid justify-content-center">
                                <div class="col-lg-10 text-center">
                                    <div id="chart_div" style="width: 100%; height: 500px;" class="card shadow"></div>
                                </div>
                            </div>
                        </div>
                        <div id="tabs-icons-text-6" role="tabpanel" aria-labelledby="tabs-icons-text-6-tab" class="tab-pane fade">
                            <div class="row row-grid justify-content-center">
                                <div class="col-lg-10 text-center">
                                    <div class="shadow-lg card">
                                        <table class="investment-statistic-list rwd-table">
                                            <thead>
                                                <tr>
                                                    <th class="text-primary">
                                                        <?php echo $text109?>
                                                    </th>
                                                    <th class="text-primary">
                                                        <?php echo $text110?> </th>
                                                    <th class="text-primary">
                                                        <?php echo $text111?><img src="./<?php echo $img; ?>/img/eth.png" alt="" width="15" style="margin-left: 1rem" class="img-fluid">
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-lg p-xs-b-0">
            <div class="container">
                <div class="row row-grid justify-content-center">
                    <div class="col-lg-8 text-center mb-4">
                        <h2 id="faq" class="display-3"><?php echo $text112?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 order-lg-2 text-center mt-5">
                        <div class="card shadow shadow-lg--hover">
                            <div class="card-body">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/zwiKmAEqDNM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 order-lg-1 mt-5">
                        <div class="text-mobile-center mt-5 mt-lg-0">
                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="faq-style">

                                        <div class="topic">
                                            <div class="open">
                                                <h2 class="question"><?php echo $text113?></h2><span class="faq-t"></span>
                                            </div>
                                            <p class="answer">
                                                <?php echo $text114?>
                                            </p>
                                        </div>
                                        <div class="topic">
                                            <div class="open">
                                                <h2 class="question"><?php echo $text115?></h2><span class="faq-t"></span>
                                            </div>
                                            <p class="answer">
                                                <?php echo $text116?>
                                            </p>
                                        </div>
                                        <div class="topic">
                                            <div class="open">
                                                <h2 class="question"><?php echo $text117?></h2><span class="faq-t"></span>
                                            </div>
                                            <p class="answer">
                                                <?php echo $text118?>
                                            </p>
                                        </div>
                                        <div class="topic">
                                            <div class="open">
                                                <h2 class="question"><?php echo $text119?></h2><span class="faq-t"></span>
                                            </div>
                                            <p class="answer">
                                                <?php echo $text120?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-lg p-xs-b-0">
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="mb-5 flex-center row row-grid justify-content-center"><a href="<? echo $bitcointalk; ?>" target="blank"><i class="fa fa-bitcoin fa-lg text-primary mb-1 fa-2x"> </i><span class="d-block mt-1">Bittalk</span></a>
                                <a href="https://etherscan.io/address/0x3da6d8f14dfcd576c46f9994d8fd94883a8f3fe8" target="blank"><i class="fa fa-etherscan fa-lg white-text fa-2x"> </i><span class="d-block">Etherscan </span></a><a onClick="gtag('event','Click2',{'event_category':'Button2','event_label':'Telegram2'});" href="https://t.me/Smart_Pyramid" target="blank"><i class="fa fa-paper-plane fa-lg white-text mb-1 fa-2x"> </i><span class="d-block mt-1">Channel</span></a><a onClick="gtag('event','Click3',{'event_category':'Button3','event_label':'Telegram3'});" href="<?php echo $chat; ?>" target="blank"><i class="fa fa-paper-plane fa-lg white-text mb-1 fa-2x">  </i><span class="d-block mt-1">Chat </span></a>
                                <a href="https://twitter.com/SmartPyramidio" target="blank"><i class="fa fa-twitter fa-lg white-text mb-1 fa-2x"> </i><span class="d-block mt-1">Twitter </span></a>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <div class="copyright mb-3">© 2018<a href="#" target="_blank"> Smart-Pyramid</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </section>
    </main>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://www.googletagmanager.com/gtag/js?id=UA-127271221-1"></script>
    

    <?php if($mode === 'debug') {  ?>

    <script type="text/javascript" src="./assets/js/logic.js?time=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="./assets/js/etherscan.js?time=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="./assets/js/infura.js?time=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="./assets/js/statistic.js?time=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="./assets/js/web3.min.js?time=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="./assets/js/init.js?time=<?php echo time(); ?>"></script>
    
    <?php } else {  ?>
    
    <script type="text/javascript" src="./dist/js/all.js"></script>
    
    <?php } ?>
    
</body>

</html>