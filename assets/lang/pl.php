﻿<?php

$work = 'The project is working';
$invest = 'Invested';
$widthdrow = 'Paid';
$peoples = 'Total Investors';
$title = 'Smart-Pyramid';
$copy= 'Skopiować';
$moreinfo = 'Szczegóły';

$text1= 'Jak to działa?';
$text2 = 'Dlaczego my';
$text3 = 'Podział';
$text4 = 'Konkurs inwestorów';
$text5 = 'Dochód <div class="display-1 d-inline-block text-primary">1,23% </div> w dzień!';
$text6='Z codziennym podwyższeniem na <div class="display-4 d-inline-block text-primary">0.01%</div> do momentu wypłaty!';
$text7='Smart kontrakt - jest to zaprogramowany tryb działań, który jest przesyłany do zdecentralizowanej sieci i staje się publicznie dostępna dla użytkowników. Kontrakt nie może zostać zmieniony po opublikowaniu w sieci.';
$text8 = 'Gwarantowano <a href="#" data-toggle="tooltip" title="'.$text7.'">smart-kontraktem </a>Ethereum';
$text9 = 'Inwestować';
$text10 = 'Żeby zrobić inwestycję w Smart-Pyramid przelejcie dowolną kwotę ETH na adres.';
$text12 = 'min. inwestycja 0.01 ETH';
$text13 = 'Polecane <a href="">ilość gas </a>- 250000';
$text14 = '*Nie wolne robić przelew z giełd! Tylko z waszego osobistego ETH portfela';
$text15 = 'Zamknąć';
$text17 = 'Dochód';
$text18 = 'Dochód/mies';
$text19 = 'Projekt działa';
$text19_1 = 'Dni';
$text20 = 'Zainwestowano';
$text21 = 'Wypłacone';
$text22 = 'Razem inwestorów';
$text23 = 'Jak to działa';
$text24 = 'Zarejestruj ETH portfel';
$text25 = 'Doładuj ETH portfel';
$text26 = 'Zrób inwestycję';
$text27 = 'Otrzymywać dochód';
$text28 = 'Polecamy posługiwać się portfelami:';
$text29 = 'Przejść na stronę <a href="https://www.metamask.io" target="_blank">metamask.io </a> i postępuj zgodnie z instrukcjami na stronie.';
$text30 = 'Po zainstalowaniu aplikacji postępuj zgodnie z instrukcjami MetaMask, aby utworzyć portfel. Upewnij się, że trzymasz wszystkie klucze w bezpiecznym miejscu.';
$text31 = 'Potem, jak portfel będzie stworzony, odnowicie tę stronę, żeby potrafiliście zrobić swoją pierwszą inwestycję.';
$text32 = 'Przejść na stronę <a href="https://www.myetherwallet.com" target="_blank">www.myetherwallet.com.</a>';
$text33 = 'Wprowadź przyszłe hasło od swojego portfela i zachowaj jego w bezpiecznym miejscu.';
$text34 = 'Postępuj zgodnie z instrukcjami ustawień swojego portfela. Upewnij się, że trzymasz wszystkie klucze w bezpiecznym miejscu.';
$text35 = 'Przejdź na stronę  <a href="https://www.bestchange.ru" target="_blank">www.bestchange.ru </a> a postępuj zgodnie z instrukcjamiи na stronie.';
$text36 = 'W lewej kolumnie wybierz walutę, w której chcesz kupić ETH. W prawej kolumnie wybierz Ethereum (ETH).';
$text37 = 'W wyświetlonym oknie wybierz najlepszą ofertę i przejdź do strony wymiany walut innej firmy.';
$text38 = 'Dokonaj wymiany za pomocą swojego osobistego portfela Ethereum.';
$text39 = 'Wybierz kwotę i wykonaj przelew';
$text40 = 'minimalma kwota dla inwestycji 0.01 ETH';
$text41 = 'Polecana <a href="">Ilość gas </a>- 250000';
$text42 = '<p class="text-small"> Jeśli robisz reinwestycję - to automatycznie biedą usunięty dywidendy,a następnie nowe dywidendy będą obliczane na podstawie nowej całkowitej inwestycji</p>';
$text43 = 'Żeby zrobić inwestycję, przelej dowolną sumę ETH pod adres smart kontraktu.:';
$text44 = 'Minimalna inwestycja 0.01 ETH';
$text45 = 'Polecana <a href="">ilość gas </a>- 250000';
$text46 = 'Otrzymuj dochód o rozmiarze <div class="display-2 d-inline-block text-primary">1.23% </div> w dzień <br>
 z codziennym zwiększeniem na  <div class="display-3 d-inline-block text-primary">0.01% </div> do momentu wycofania!';
$text47 = 'Po otrzymaniu wypłaty, współczynnik dochodowości powraca do <b>1.23%</b></p>';
$text48 = 'Dostać wypłatę możne w dowolnym czasie, ale nie częściej niż raz w 10 minut. Przy tym twój dochód zostanie obliczony na podstawie czasu, jaki upłynął od ostatniego wypłaty lub od czasu ostatniej inwestycji.';
$text49 = 'Zlecenie na wypłaty, naciśnij:';
$text50 = 'Dostać wypłatę';
$text51 = 'Przy tym należy potwierdzić wysłanie ETH na adres smart kontraktu';
$text53 = 'W przypadku zlecenia wypłaty, wyslij 0 ETH na adres smart kontraktu:';
$text55 = 'Jak to działa';
$text56 = 'Zarejestrować ETH portfel';
$text57 = 'Polecamy posługiwać się portfelami:';
$text58 = 'Skopiowano!';
$text59 = 'Dlaczego my?';
$text60 = 'Zwykła piramida';
$text61 = 'Administratorzy w dowolnym momencie mogą zamknąć projekt, inwestorzy zostają nie z czym';
$text62 = 'Podczas łamania piramidy wszystkie środki są bezpowrotnie utracone.';
$text63 = 'Wszystkie informacje o balansie piramidy są ukryte.';
$text64 = 'Administracja może zablokować niepożądanych inwestorów, pobierając z nich pieniądze ';
$text65 = 'Wypłaty podlegają wyłącznie Smart kontraktu Ethereum!';
$text66 = 'Otwórz niezawodny kod, którego nie można złamać. Audyt kodu był wielokrotnie przeprowadzany przez niezależnych ekspertów.';
$text67 = 'Wyniki audytu';
$text68 = 'W pełni <a href="https://etherscan.io/address/0x3da6d8f14dfcd576c46f9994d8fd94883a8f3fe8" target="blank">Otwórz statystyki na blockchain Ethereum</a> dostępne każdemu!';
$text69 = 'Nikt nie może zabronić ci otrzymywania codziennych dywidend.';
$text70 = 'Podział środków';
$text71 = '84%<p>idzie na wypłaty</p>';
$text72 = '11%<p>idzie na marketing</p>';
$text73 = '3%<p>komisja administracji</p>';
$text74 = '2%<p>Obsługa techniczna i wypłaty</p>';
$text75 = 'Konkurs inwestorów';
$text76 = 'Co poniedziałek odbywa się konkurs na smart kontrakcie, według którego odbywa się podział<span class="display-4 text-primary">2, 1 </span>i <span class="display-4 text-primary">0.5 ETH </span> trzem portfelom, który zrobiły największe inwestycje za poprzedni tydzień!';
$text77 = 'Do końca konkursu zostało:';
$text78 = 'Dni';
$text79 = 'Godzin';
$text80 = 'Minut';
$text81 = 'Sekund';
$text82 = 'Terażniejsze zwycięzce';
$text83 = 'Przeszłe zwycięzce';
$text84 = 'PS W przypadku inwestycji o różnych portfelach tej samej kwoty, najwyższe miejsce przyznawane jest portfelowi, który dokonał inwestycji wcześniej niż inne';
$text85 = 'Wyniki konkursu podsumowane co poniedziałek 13:00. Po nadejściu tego czasu, dowolny użytkownik sam może inicjować wykonanie konkursu. Dla tego należy zainstalować aplikacje Metamask. Wejść na <a href="https://etherscan.io/address/0x3da6d8f14dfcd576c46f9994d8fd94883a8f3fe8">SMART KONTRAKT</a> , dalej przejść do wkładki Whire Contract i przyciśnij payDay. Nagrody automatycznie będą obesłały i zacznie się nowy konkurs inwestorów do następnego Poniedziałku';
$text86 = 'Program partnerski';
$text87 = 'Dostań <div class="display-2 d-inline-block text-primary">5% </div> referalnych, zapraszając nowych uczestników!';
$text88 = 'Dostań ref. link';
$text89 = 'Wprowadz adres portfelu';
$text90 = 'Wygenerować';
$text91 = 'Twój link referalny:';
$text92 = 'Materiały reklamowe';
$text93 = 'Link na reklame:';
$text94 = 'Środki rozdziela smart kontrakt w momencie, gdy postępuje inwestycja';
$text95 = '<p class="text-small">PS Przy opłacie MyEtherWallet referal musi wprowadzić twój portfel w oknie DATA</p>';
$text96 = 'Twoje konto';
$text97 = 'Możecie zobaczyć całą informację o Waszych inwestycjach, Waszych wypłatach i referalach.<br> Wprowadź adres, z którego robiłeś inwestycję';
$text97_1 = 'Wprowadź adres';
$text98 = 'Pokaż';
$text99 = 'Twoje inwestycje';
$text100 = 'Razem';
$text101 = 'Twój dochód';
$text102 = 'Blok';
$text103 = 'Typ';
$text104 = 'Twoje referaly';
$text105 = 'Adres';
$text106 = 'Statystyka';
$text107 = 'Grafik';
$text108 = 'Tranzakcja';
$text109 = 'Data(UTC)';
$text110 = 'Adres';
$text111 = 'Kwota';
$text112 = 'Często zadawane pytania';

$text113 = 'Co się stanie z projektem, jeśli strona przestanie działać?';
$text114 = 'Strona internetowa ma wielką informacyjną funkcję. Wszystkie funkcje projektu w całości są autonomiczne! Jeśli z jakiegokolwiek powodu strona internetowa przestane działać - to w żaden sposób nie wpłynie to na wypłatę dywidendy i przyjęcie wkładów.';
$text115 = 'Kto czyni proces podziału wynagrodzeniu i dywidend?';
$text116 = 'Automatyzowany system smart kontrakt będzie prowadził rozliczenia wypłat bez uczestnictwa trzecich osób';
$text117 = 'Jakie są gwarancje?';
$text118 = 'Wypłaty są gwarantowane przez sieć Ethereum, założyciele projektu nie mają wpływu na pracę.';
$text119 = 'Jak długo będzie działał projekt?';
$text120 = 'Na razie istniejeeje sieć Ethereum';

$text121 = 'Wypłatę można będzie otrzymywać dopóki  balans smatr kontraktu jest na plusie!';

$text122 = 'Później wycofujesz - więcej dochodu!
Z każdym dniem współczynnik dochodowości rośnie na 0.01%, dopóki nie zaprosisz wypłatę. Potem, współczynnik dochodowości spada do 1.23% i znów zaczyna rosnąć w każdym dniem.';

$text42_2='Wasza inwestycja jest zgodą z wszystkimi warunkami <a href="/EULO.pdf" target="blank">Umowy użytkownika</a>'; 
$data_text = 'Przy obecności pola <font color="#8a2be2">DATA</font>, wprowadż: ';

$chat = 'https://t.me/joinchat/I-apLQ9CaiRMMc_yWcSP2Q';
$bitcointalk = 'https://bitcointalk.org/index.php?topic=5049864';
 ?>
