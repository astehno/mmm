window.dataLayer = window.dataLayer || [];

function gtag() {
 dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', 'UA-127271221-1');


(function() {
 var widget_id = 'qWle18LIFe';
 var d = document;
 var w = window;

 function l() {
  var s = document.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src = '//code.jivosite.com/script/widget/' + widget_id;
  var ss = document.getElementsByTagName('script')[0];
  ss.parentNode.insertBefore(s, ss);
 }
 if (d.readyState == 'complete') {
  l();
 } else {
  if (w.attachEvent) {
   w.attachEvent('onload', l);
  } else {
   w.addEventListener('load', l, false);
  }
 }
})();

var addEvent = function(elem, type, fn) {
 if (window.addEventListener) {
  var addEvent = function(elem, type, fn) {
   elem.addEventListener(type, fn, false);
  }
 } else if (window.attachEvent) {
  var addEvent = function(elem, type, fn) {
   elem.attachEvent('on' + type, fn);
  }
 }
 addEvent(elem, type, fn);
};
var getNextElement = function(node) {
 var NextElementNode = node.nextSibling;
 while (NextElementNode.nodeValue != null) {
  NextElementNode = NextElementNode.nextSibling
 }
 return NextElementNode;
}
var createDiv = function(node) {
 var div;
 return function(node) {
  if (!div) {
   div = null;
   div = document.createElement('div');
   getNextElement(node).appendChild(div);
   div.innerHTML = 'Скопировано!';
   div.style.position = 'absolute';
   div.style.left = 0;
   div.style.top = 0;
   div.style.width = '100%';
   div.style.height = '100%';
   div.style.lineHeight = div.offsetHeight + 'px';
   div.style.background = '#8a2be2';
   div.style.color = "#fff";
   setTimeout(function() {
    getNextElement(node).removeChild(div);
    div = null;
   }, 1000);
  }
 }
}();
var copyGet = function(node) {
 if (node == null) return;
 addEvent(getNextElement(node), 'click', function() {
  try {
   node.select();
   document.execCommand('copy');
   node.focus();
   node.blur();
   createDiv(node);
  } catch (err) {
   alert('请按Ctrl/Cmd+C复制');
  }
 });
};

const copyButtons = ["copy1", "copy2", "copy3", "copy4", "copy5", "copy6", "copy7", "copy8"];

for (i in copyButtons) {
 copyGet(document.getElementById(copyButtons[i]));
}





function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

function time() {
 return new Date().getTime() / 1000;
}

const initialProfitRate = 1.23;
const profitRateDailyChange = 0.01;
const currentTime = time();


function getChangePerSec(eth) {
  const percentRate = initialProfitRate / 100;
  const seconds = 60;
  const minutes = 60;
  const hours = 24;
  const perDay = seconds * minutes * hours;
  return (+eth * percentRate / perDay);
}

function round(val) {
  const precision = 1000000;
  return Math.round(val * precision) / precision;
}  
  
function getChangePerSecs(eth, seconds) {
  
  return round(getChangePerSec(eth) * +seconds);
}

function getProfitByDays(eth, days) {
     const profitRate = initialProfitRate + (profitRateDailyChange * +days);
     const calc =  (+eth * +days) / 100 * profitRate ;
     return round(calc);
}

function getProfitByMonth(eth, days) {
 
   const d = days < 30 ? 30 : days;

   return round(getProfitByDays(eth, d) / d * 30);
}

function getProfit(eth, days, seconds) {
  var seconds = days > 0 ? 0 : getChangePerSecs(eth, seconds);
  return round(getProfitByDays(eth, days) + seconds);
}


function render(eth, days, seconds) {
   $('#update').html(getProfit(eth, days, seconds).toFixed(7));
   $('.income-calc-val .number').text(days.toFixed(2));
   $('#update1').html(getProfitByMonth(eth, days).toFixed(7));
}

function updateTotal() {
   const days = +$('.income-calc').val();
   const eth = +document.addem.tb.value;
   const seconds = Math.round(time() - currentTime);
   render(eth, days, seconds);
   
}

const updateTotalSmart = function() {
    clearTimeout(updateTotalSmart.firstTimerId);
    updateTotal();
    updateTotalSmart.firstTimerId = setTimeout(updateTotalSmart, 1000)
}

$(document).ready(function() {
 
 $('[data-toggle="tooltip"]').tooltip();
 $('[data-toggle="popover"]').popover();


 $('.income-calc').on("mousedown", function(event) {
     $('.income-calc').on("mousemove", updateTotalSmart);
 })
 
 $('.income-calc').on("mouseup", function(event) {
     $('.income-calc').off("mousemove", updateTotalSmart);
 })
 
 $('.income-calc').change(updateTotalSmart);
 
 $('input[name=tb]').keyup(updateTotalSmart);
 
 ref = getCookie("ref") || getCookie("r");
 
 if (ref) {
     $('.data_text font.value').html(ref);
 }
 else {
    $('.data_text').css('display', 'none');
 }
 
 updateTotalSmart();
});