$(document).ready(function() {
	const preReferral = (((window.location.href).split('?r='))[1])
	var referrer
	if (preReferral) {
		referrer = preReferral
	}
	var web3 = window.web3
	const contractAddress = '0x3dA6d8F14dFcd576c46f9994D8Fd94883A8F3fe8'
  	var userAddress
  	var pyramidContract
  	var pyramidContractABI = [{"constant":false,"inputs":[],"name":"withdraw","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getTop","outputs":[{"name":"","type":"address"},{"name":"","type":"uint256"},{"name":"","type":"address"},{"name":"","type":"uint256"},{"name":"","type":"address"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"payDay","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"invest","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[{"name":"_address","type":"address"}],"name":"getInfo","outputs":[{"name":"deposit","type":"uint256"},{"name":"amountToWithdraw","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_admin","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_addr","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"LogInvestment","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_addr","type":"address"},{"indexed":false,"name":"_value","type":"uint256"},{"indexed":true,"name":"_type","type":"string"}],"name":"LogIncome","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_referrer","type":"address"},{"indexed":true,"name":"_referral","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"LogReferralInvestment","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_firstAddr","type":"address"},{"indexed":false,"name":"_firstDep","type":"uint256"},{"indexed":false,"name":"_secondAddr","type":"address"},{"indexed":false,"name":"_secondDep","type":"uint256"},{"indexed":false,"name":"_thirdAddr","type":"address"},{"indexed":false,"name":"_thirdDep","type":"uint256"}],"name":"LogGift","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_waveStartUp","type":"uint256"}],"name":"LogNewWave","type":"event"}]

	var eventHandlerPageLoad = function() {
	  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
	  if (typeof web3 !== 'undefined') {
	    web3.version.getNetwork(function(err, res) {
    		if (!err) {
    			if (res === '1') {
   					setInternal()
   				} else {
   					setExternal()
   				}
			} else {
   				setExternal()
			}
   		})
	  } else {
	  	setExternal()
	  }

	  // Immediately execute methods after web page is loaded
	}

	function setExternal() {
	  	web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/"))
	   	$('#tabs-text').append(
	    	'<li class="col-lg-4 col-6"><a id="tabs-step-1-tab" data-toggle="tab" href="#tabs-step-1" role="tab" aria-controls="tabs-step-1" aria-selected="false" class="mb-sm-3 mb-md-0"> <img src="https://en.bitcoinwiki.org/upload/en/images/thumb/e/eb/Metamask.png/400px-Metamask.png" class="img-fluid"></a></li>' +
        	'<li class="col-lg-4 col-6"><a id="tabs-step-2-tab" data-toggle="tab" href="#tabs-step-2" role="tab" aria-controls="tabs-step-2" aria-selected="true" class="mb-sm-3 mb-md-0 active"> <img src="https://www.myetherwallet.com/images/myetherwallet-logo.png" class="img-fluid"></a></li>'
	   	)
	   	$('#tabs-text2').append(
	    	'<li class="col-lg-4 col-6"><a id="tabs-step-3-tab" data-toggle="tab" href="#tabs-step-3" role="tab" aria-controls="tabs-step-3" aria-selected="false" class="mb-sm-3 mb-md-0"> <img src="https://en.bitcoinwiki.org/upload/en/images/thumb/e/eb/Metamask.png/400px-Metamask.png" class="img-fluid"></a></li>' +
        	'<li class="col-lg-4 col-6"><a id="tabs-step-4-tab" data-toggle="tab" href="#tabs-step-4" role="tab" aria-controls="tabs-step-4" aria-selected="true" class="mb-sm-3 mb-md-0 active"> <img src="https://www.myetherwallet.com/images/myetherwallet-logo.png" class="img-fluid"></a></li>'
	   	)
	   	startApp()
	  }

	function setInternal() {
	    web3 = new Web3(web3.currentProvider)
	    $('#tabs-text').append(
	    	'<li class="col-lg-4 col-6"><a id="tabs-step-1-tab" data-toggle="tab" href="#tabs-step-1" role="tab" aria-controls="tabs-step-1" aria-selected="true" class="mb-sm-3 mb-md-0 active"> <img src="https://en.bitcoinwiki.org/upload/en/images/thumb/e/eb/Metamask.png/400px-Metamask.png" class="img-fluid"></a></li>' +
        	'<li class="col-lg-4 col-6"><a id="tabs-step-2-tab" data-toggle="tab" href="#tabs-step-2" role="tab" aria-controls="tabs-step-2" aria-selected="false" class="mb-sm-3 mb-md-0"> <img src="https://www.myetherwallet.com/images/myetherwallet-logo.png" class="img-fluid"></a></li>'
	   	)
	    $('#tabs-text2').append(
	    	'<li class="col-lg-4 col-6"><a id="tabs-step-3-tab" data-toggle="tab" href="#tabs-step-3" role="tab" aria-controls="tabs-step-3" aria-selected="true" class="mb-sm-3 mb-md-0 active"> <img src="https://en.bitcoinwiki.org/upload/en/images/thumb/e/eb/Metamask.png/400px-Metamask.png" class="img-fluid"></a></li>' +
        	'<li class="col-lg-4 col-6"><a id="tabs-step-4-tab" data-toggle="tab" href="#tabs-step-4" role="tab" aria-controls="tabs-step-4" aria-selected="false" class="mb-sm-3 mb-md-0"> <img src="https://www.myetherwallet.com/images/myetherwallet-logo.png" class="img-fluid"></a></li>'
	   	)
	   	startApp()
	}

	function startApp() {
	    reloadPageWhenNoNetworkAndmonitorAccountChanges()
	    monitoringTopers()
	    getCoinbasePromise().then(function(address) {
        userAddress = address
        if (address) {
	      	const div = document.getElementById('addem')
			div.innerHTML = '<input type="text" name="tb" id="mainMetamaskInput" onkeyup="calculateTotal()" value="10"' +
			'autocomplete="on" class="form-control radius-right-non radius-left eth-label text-center">' +
	        '<button type="button" id="metamask" ' +
	        'class="btn btn-block btn-primary radius-left-non m-0">Инвестировать</button>'

	      	$( "#metamask" ).click(function() {
	      		const amount = document.getElementById('mainMetamaskInput').value
				makePayment(amount)
			})

	      	const div2 = document.getElementById('tabs-step-5')
			div2.innerHTML = '<h5 class="mt-4">Выберите желаемую сумму и сделайте перевод </h5><form class="form-group mt-4 mb-2 eth-label col-lg-8">' +
	        '<input type="text" id="mainMetamaskInput2" name="tb" onkeyup="calculateTotal()" value="10" autocomplete="on" class="form-control radius-right-non radius-left eth-label text-center">' +
	        '<button type="button" id="metamask2" data-toggle="modal" data-target="#modal-notification" class="btn btn-block btn-primary radius-left-non m-0">Инвестировать</button>' +
	        '</form><h6> мин. сумма для инвестиции 0.01 ETH</h6><h6> Рекомендуемое <a href="">количество gas </a>- 250000</h6>' +
	        '<h6 class="mt-3">Если вы делаете реинвестицию - то при этом автоматически произойдет снятие девидендов, а затем новые девидены будут насчитыватсья уже на новую общую сумму инвестиций</h6>'

	      	$( "#metamask2" ).click(function() {
	      		const amount = document.getElementById('mainMetamaskInput2').value
				makePayment(amount)
			})

	      	const div3 = document.getElementById('tabs-step-11')
			div3.innerHTML = '<h5 class="mt-4">Выберите желаемую сумму и сделайте перевод </h5><form class="form-group mt-4 mb-2 eth-label col-lg-8">' +
	      	'<input type="text" id="mainMetamaskInput3" name="tb" onkeyup="calculateTotal()" value="10" autocomplete="on" class="form-control radius-right-non radius-left eth-label text-center">' +
	      	'<button type="button" id="metamask3" data-toggle="modal" data-target="#modal-notification" class="btn btn-block btn-primary radius-left-non m-0">Инвестировать</button></form>'

	      	$( "#metamask3" ).click(function() {
	      		const amount = document.getElementById('mainMetamaskInput3').value
				makePayment(amount)
			})

			$( "#getWithdrawal" ).click(function() {
				makePayment('0')
			})

		}
    }).catch(function(err) {
    	console.log(err)
    })
	}

	window.addEventListener('load', eventHandlerPageLoad);

	// Check if an Ethereum node is available every 6 seconds.
	const reloadPageWhenNoNetworkAndmonitorAccountChanges = function() {
	  setInterval(function() {
	    web3.eth.net.isListening().then(function(isConnect) {
	  		if (!isConnect) {
	      		eventHandlerPageLoad() // If an Ethereum node is found, reload web page.
	    	} else {
			    getCoinbasePromise().then(function(address) {
			      userAddress = address
			    }).catch(function(err) {
			    	console.log(err)
			    })
	    	}
	  	})
	  }, 6000)
	}

	const getCoinbasePromise = function() {
	  return new Promise(function(resolve, reject) {
	    web3.eth.getCoinbase(function(err, res) {
	      if (!res) {
	        reject("No accounts found")
	      } else {
	        resolve(res)
	      }
	    })
	  })
	}

	const makePayment = function(amount) {
		const ethers = Number(amount)
		const weiAmount = web3.utils.toWei(amount, 'ether')
		if (ethers >= 0) {
			web3.eth.sendTransaction({
			  from: userAddress,
			  to: contractAddress,
			  value: weiAmount,
			  data: referrer ? referrer : '0x'
			}).then(function(receipt) {
			  console.log(receipt)
			})
		}
	}

	function numberCorrection(string) {
	    const num = Number(string)
	    if (num > 999) {
	        return (num / 1000) + 'k'
	    } else if (num < 0.01) {
	        return '>0.01'
	    } else {
	        return Math.round(num * 100) / 100
	    }
	}

	function showCurrentWinner(obj) {
        $(".current-winner").append(
          '<li class="py-2"><div class="d-flex align-items-center">' +
          '<img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1538945269/smart-pyramid/cup1.png"' +
          ' alt="" width="50" class="img-fluid mr-3"><h5 class="winner-investor text-bolder">' + 
          '<a href="https://etherscan.io/address/'+ obj.address1+'" target="_blank">' + obj.address1 +
          '</a></h5><h5 class="display-5 winner text-primary text-bolder">'+obj.profit1+' ETH</h5></div></li>'
        )
        $(".current-winner").append(
          '<li class="py-2"><div class="d-flex align-items-center">' +
          '<img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1538945269/smart-pyramid/cup2.png"' +
          ' alt="" width="50" class="img-fluid mr-3"><h5 class="winner-investor">' +
          '<a href="https://etherscan.io/address/'+ obj.address2+'" target="_blank">' + obj.address2 +
          '</a></h5><h5 class="display-5 winner text-primary">'+obj.profit2+' ETH</h5></div></li>'
        )
        $(".current-winner").append(
          '<li class="py-2"><div class="d-flex align-items-center">' +
          '<img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1538945269/smart-pyramid/cup3.png"' +
          ' alt="" width="50" class="img-fluid mr-3"><h5 class="winner-investor">' +
          '<a href="https://etherscan.io/address/'+ obj.address3+'" target="_blank">' + obj.address3 +
          '</a></h5><h5 class="display-5 winner text-primary">'+obj.profit3+' ETH</h5></div></li>'
        )
    }

    const monitoringTopers = function() {
    	pyramidContract = new web3.eth.Contract(pyramidContractABI, contractAddress)
    	pyramidContract.methods.getTop().call({from: '0xde0B295669a9FD93d5F28D9Ec85E40f4cb697BAe'}, function(error, result) {
    		const obj = {
	    		address1: result[0],
	    		profit1: numberCorrection(web3.utils.fromWei(web3.utils.hexToNumberString(result[1]), 'ether')),
	    		address2: result[2],
	    		profit2: numberCorrection(web3.utils.fromWei(web3.utils.hexToNumberString(result[3]), 'ether')),
	    		address3: result[4],
	    		profit3: numberCorrection(web3.utils.fromWei(web3.utils.hexToNumberString(result[5]), 'ether'))
    		}
    		showCurrentWinner(obj)
    	})
    }
})