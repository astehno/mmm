$(document).ready(function() {
  const contractAddress = '0x3dA6d8F14dFcd576c46f9994D8Fd94883A8F3fe8'
  const fromContractBlock = '6500668'
  const fromContractTimestamp = '1539340144'
  window.web3e = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/"))
    
  function numberCorrection(string) {
    const num = Number(string)
    if (num > 999) {
      return (num / 1000) + 'k'
    } else if (num < 0.01) {
      return '>0.01'
    } else {
      return Math.round(num * 100) / 100
    }
  }

  // generate link
  $( "#referralAddressButton" ).click(function() {
    const value = document.getElementById('referralAddress').value
    $('.referralLink').val('https://smart-pyramid.io/?r=' + value)
  })

    const eventWin = '0x6086647a3c64e449d27817e6a917a85257a24416079e5c468001990a58498cb1'
    const getWinners = 'https://api.etherscan.io/api?module=logs' +
    '&action=getLogs&fromBlock=' + fromContractBlock +
    '&toBlock=latest&address=' + contractAddress +
    '&topic0=' + eventWin + '&apikey=YourApiKeyToken'

    // winners history
    $.get( getWinners, function (data) {
      if (!data || !data.status === '1') console.log('Etherscan troubles')
      const array = data.result.reverse()

      function getObject(element) {
        var address1 = address2 = address3 = profit1 = profit2 = profit3 = ''
        const obj = {}
        const stringData = array[element].data.slice(2)
        for ( j = 0; j < 6; j++ ) {
          const item = stringData.slice(64 * j, 64 * (j + 1))
          if (j === 0) {
            obj.address1 = '0x' + item.slice(24)
          } else if (j === 1) {
            obj.profit1 = numberCorrection(web3e.utils.fromWei(web3e.utils.hexToNumberString(item), 'ether'))
          } else if (j === 2) {
            obj.address2 = '0x' + item.slice(24)
          } else if (j === 3) {
            obj.profit2 = numberCorrection(web3e.utils.fromWei(web3e.utils.hexToNumberString(item), 'ether'))
          } else if (j === 4) {
            obj.address3 = '0x' + item.slice(24)
          } else if (j === 5) {
            obj.profit3 = numberCorrection(web3e.utils.fromWei(web3e.utils.hexToNumberString(item), 'ether'))
          }
        }

        const timestamp = web3e.utils.hexToNumber(array[i].timeStamp)

        const date = new Date(timestamp * 1000)
        obj.day = date.getUTCDate()
        obj.month = date.getMonth() + 1
        obj.year = date.getFullYear()

        return obj
      }

      function showPastWinner(obj) {
        $(".past-winner").append(
          '<li class="py-2 text-center"><span>'+obj.day+'/'+obj.month+'/'+obj.year+'</span></li>' +
          '<li class="py-2"><span class="d-flex align-items-center"><img' +
          ' src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1538945269/smart-pyramid/cup1.png"' +
          ' alt="" width="30" class="img-fluid mr-3"><h5 class="p-0 mb-0 winner-adress text-bolder">' +
          '<a href="https://etherscan.io/address/'+obj.address1+'" target="_blank">'+obj.address1+'</a></h5>' +
          '<h5 class="p-0 mb-0 text-primary text-right text-bolder">'+obj.profit1+' ETH</h5></span></li>'
        )
        $(".past-winner").append(
          '<li class="py-2 text-center"></li><li class="py-2"><span class="d-flex align-items-center"><img' +
          ' src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1538945269/smart-pyramid/cup2.png"' +
          ' alt="" width="30" class="img-fluid mr-3"><h5 class="p-0 mb-0 winner-adress">' +
          '<a href="https://etherscan.io/address/'+obj.address2+'" target="_blank">'+obj.address2+'</a></h5>' +
          '<h5 class="p-0 mb-0 text-primary text-right">'+obj.profit2+' ETH</h5></span></li>'
        )
        $(".past-winner").append(
          '<li class="py-2 text-center"></li><li class="py-2"><span class="d-flex align-items-center"><img' +
          ' src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1538945269/smart-pyramid/cup3.png"' +
          ' alt="" width="30" class="img-fluid mr-3"><h5 class="p-0 mb-0 winner-adress">' +
          '<a href="https://etherscan.io/address/'+obj.address3+'" target="_blank">'+obj.address3+'</a></h5>' +
          '<h5 class="p-0 mb-0 text-primary text-right">'+obj.profit3+' ETH</h5></span></li>'
        )
      }

      for ( i = 0; i < array.length; i++ ) {
        if (array[i]) {
          const data = getObject(i)
          showPastWinner(data)
        }
      }
    })

  $( "#showUserInfo" ).click(function() {
    $('.userInvestment').html(
      '<thead><tr><th class="text-primary">Блок</th>' +
      '<th class="text-primary"><img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1538680384/smart-pyramid/eth.png" alt="" width="15" class="img-fluid"></th>' +
      '</tr></thead><tbody></tbody><tfoot></tfoot>'       
    )
    $('.userIncome').html(
      '<thead><tr><th class="text-primary">Блок</th><th class="text-primary">Тип</th>' +
      '<th class="text-primary"><img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1538680384/smart-pyramid/eth.png" alt="" width="15" class="img-fluid"></th>' +
      '</tr></thead><tbody></tbody><tfoot></tfoot>'
    )
    $('.userReferrals').html(
      '<thead><tr><th class="text-primary">Адрес</th>' +
      '<th class="text-primary"><img src="https://res.cloudinary.com/dfbhd7liw/image/upload/v1538680384/smart-pyramid/eth.png" alt="" width="15" class="img-fluid"></th>' +
      '</tr></thead><tbody></tbody><tfoot></tfoot>'        
    )
    userAddress = document.getElementById('userInfoAddressInput').value
    address = '0x000000000000000000000000' + userAddress.slice(2)
    getInfo(address)
  })

  const getInfo = function(address) {
    const eventInvestment = '0xc74590e3281392e897f5c0f45530951cfe0db0e86c76d65af861e80b925871a4'
    const getInvestment = 'https://api.etherscan.io/api?module=logs' +
    '&action=getLogs&fromBlock=' + fromContractBlock +
    '&toBlock=latest&address=' + contractAddress +
    '&topic0=' + eventInvestment + '&topic1=' + address + '&apikey=YourApiKeyToken'

    const eventIncome = '0x0c57a5d4dcad8ee459870d907ba5bc2db98361a795625c039b057de4e923d197'
    const getIncome = 'https://api.etherscan.io/api?module=logs' +
    '&action=getLogs&fromBlock=' + fromContractBlock +
    '&toBlock=latest&address=' + contractAddress +
    '&topic0=' + eventIncome + '&topic1=' + address + '&apikey=YourApiKeyToken'

    const eventReferralInvestment = '0xa249146257bee059355926b54611f49f096a7b1ed415e8011b89838f96e5fc51'
    const getReferralInvestment = 'https://api.etherscan.io/api?module=logs' +
    '&action=getLogs&fromBlock=' + fromContractBlock +
    '&toBlock=latest&address=' + contractAddress +
    '&topic0=' + eventReferralInvestment + '&topic1=' + address + '&apikey=YourApiKeyToken'

    // user investment history
    $.get( getInvestment, function (data) {
      if (!data || !data.status === '1') console.log('Etherscan troubles')
      const array = data.result.reverse()
      var total = 0

      for ( i = 0; i < array.length; i++ ) {
        if (array[i]) {
          const block = web3e.utils.hexToNumber(array[i].blockNumber)
          const amount = (web3e.utils.fromWei(web3e.utils.hexToNumberString(array[i].data.slice(2)), 'ether'))
          total = total + Number(amount)
          $('.userInvestment').append(
            '<tr><td data-th="Блок" class="text-primary">' +
            '<a href="https://etherscan.io/block/'+block+'" target="_blank">'+block+'</a></td>' +
            '</td><td data-th="ETH">' + numberCorrection(amount) + '</td></tr>'
          )
        }
      }

      $('.userInvestment').append(
        '<tr><td data-th="Всего" class="text-primary last-child">Всего' + 
        '</td><td data-th="ETH" class="last-child">' + numberCorrection(total) + '</td></tr>'
      )
    })

    // user income history
    $.get( getIncome, function (data) {
      if (!data || !data.status === '1') console.log('Etherscan troubles')
      const array = data.result.reverse()
      var total = 0

      for ( i = 0; i < array.length; i++ ) {
        if (array[i]) {
          const block = web3e.utils.hexToNumberString(array[i].blockNumber)

          const amountRaw = array[i].data.slice(2, 66)
          const amount = web3e.utils.fromWei(web3e.utils.hexToNumberString(amountRaw), 'ether')
          var type

          if (array[i].topics[2] === '0xfe7a30932c603073539b5550117ca9b29d73e38ae68d5e096d811e6cb3eaa71f') {
            type = 'referral'
          } else if (array[i].topics[2] === '0xcd13ac303d5d39abe59dadbd9939c261224cd9968056e630c4472fb131ab2f6c') {
            type = 'withdrawal'
          } else {
            type = 'Unknown'
          }
          console.log(type)
          console.log(array[i].transactionHash)
          console.log(array[i].topics[2])

          $('.userIncome').append(
            '<tr><td data-th="Блок" class="text-primary">' +
            '<a href="https://etherscan.io/block/'+block+'" target="_blank">'+block+'</a></td>' +
            '<td data-th="Тип">' + type + '</td><td data-th="ETH">' + numberCorrection(amount) + '</td></tr>'
          )

          total = total + Number(amount)
        }
      }

      $('.userIncome').append(
        '<tr><td data-th="Всего" class="text-primary last-child">Всего</td>' +
        '<td data-th="Тип" class="last-child"> </td><td data-th="ETH" class="last-child">' + numberCorrection(total) + '</td></tr>'
      )
    })

    // user referrals
    $.get( getReferralInvestment, function (data) {
      if (!data || !data.status === '1') console.log('Etherscan troubles')
      const array = data.result.reverse()
      var total = 0

      for ( i = 0; i < array.length; i++ ) {
        if (array[i]) {
          const referral = '0x' + array[i].topics[2].slice(26,66)
          const amount = web3e.utils.fromWei(web3e.utils.hexToNumberString(array[i].data.slice(2)), 'ether')
          total = total + Number(amount)
          $('.userReferrals').append(
            '<tr><td data-th="Адрес" class="text-primary short-adress"><a href="https://etherscan.io/address/' + 
            referral + '" target="_blank">' + referral + '</a></td><td data-th="ETH">' + numberCorrection(amount) + '</td></tr>'
          )
        }
      }

      $('.userReferrals').append(
        '<tr><td data-th="Всего" class="text-primary last-child">Всего</td>' +
        '<td data-th="ETH" class="last-child">' + numberCorrection(total) + '</td></tr>'
      )
    })
  }
})