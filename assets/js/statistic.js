$(document).ready(function() {
	// countdown
	const second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24;

    const currenctTimestamp = (+ new Date()) / 1000
    var countTimeStamp = 1539597600
    if (currenctTimestamp > countTimeStamp) {
    	countTimeStamp = countTimeStamp + 604800
    }

    // if (countTimeStamp)
	var countDown = new Date(countTimeStamp * 1000).getTime(),
  	x = setInterval(function() {
  		var now = new Date().getTime(),
  		distance = countDown - now

      document.getElementById('days').innerText = Math.floor(distance / (day)),
      document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
      document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
      document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
    }, second)


  	// utils
	const contractAddress = '0x3dA6d8F14dFcd576c46f9994D8Fd94883A8F3fe8'
	const fromContractBlock = '6500668'
	const fromContractTimestamp = '1539260372'

	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

	// something was wrong with web3 now) create one more)
	window.web33 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/"))

    const current = + (new Date() / 1000)
    const days = Math.floor((current - fromContractTimestamp) / (60 * 60 * 24))
    $('.day-counter-statistic').append(
      '<h6 class="display-3 statistic-counter">' + days + '</h6><span class="label">Дней</span>'
    )

    const eventInvestment = '0xc74590e3281392e897f5c0f45530951cfe0db0e86c76d65af861e80b925871a4'
    const getInvestment = 'https://api.etherscan.io/api?module=logs' +
    '&action=getLogs&fromBlock=' + fromContractBlock +
    '&toBlock=latest&address=' + contractAddress +
    '&topic0=' + eventInvestment + '&apikey=YourApiKeyToken'

    // get json data
    $.get( getInvestment, function (data) {
      if (!data || !data.status === '1') console.log('Etherscan troubles')

      const array = data.result
      var total = 0
      var addressArray = []
      var chartArray = [['Year', 'ETH', 'Investors'],['11. Oct', 0, 0]]
      var currenctDay = 0
      for ( i = 0; i < array.length; i++ ) {
        if (array[i]) {
          const time = new Date(web33.utils.hexToNumber(array[i].timeStamp) * 1000)
          const timeObj = {
          	minutes: time.getMinutes(),
          	hour: time.getUTCHours(),
	          day: time.getUTCDate(),
            month: time.getMonth() + 1,
        	  year: time.getFullYear()
    	    }
          
          const amount = web33.utils.fromWei(web33.utils.hexToNumberString(array[i].data.slice(2)), 'ether')
          total = total + Number(amount)
          const address = '0x' + array[i].topics[1].slice(26,66)
          if (!addressArray.find(function(itemAddress) { return itemAddress === address })) addressArray.push(address)

          if (currenctDay !== timeObj.day) {
            if (chartArray.find(function(itemArray) {
            	if (itemArray[0] !== 'Year') {
            		const dateFromChart = itemArray[0].split('. ')
            		return (Number(dateFromChart[0]) !== timeObj.day) || (dateFromChart[1] !== monthNames[timeObj.month - 1])
            	}
            })) {
            	chartArray.push([timeObj.day+'. '+monthNames[timeObj.month - 1], Math.round(total * 100) / 100, addressArray.length])
              currenctDay = timeObj.day
            }
          }
        }
      }

      drawInvestmentChart(chartArray)

      const arrayReverse = data.result.reverse()
      for ( i = 0; i < 8; i++ ) {
        if (arrayReverse[i]) {
          const time = new Date(web33.utils.hexToNumber(arrayReverse[i].timeStamp) * 1000)
          const timeObj = {
            minutes: time.getMinutes(),
            hour: time.getUTCHours(),
            day: time.getUTCDate(),
            month: time.getMonth() + 1,
            year: time.getFullYear()
          }
          
          const amount = web33.utils.fromWei(web33.utils.hexToNumberString(arrayReverse[i].data.slice(2)), 'ether')
          const address = '0x' + array[i].topics[1].slice(26,66)
          
          $('.investment-statistic-list').append(
            '<tr><td data-th="Дата" class="text-primary">'+timeObj.day+'.'+timeObj.month+'.'+timeObj.year+' ('+timeObj.hour+':'+timeObj.minutes+')</td>' +
            '<td data-th="Адрес" class="short-adress"><a href="https://etherscan.io/address/'+ address +'" ' +
            'target="_blank">'+address+'</a></td><td data-th="Сумма ETH">'+ amount +'</td></tr>'
          )
        }
      }

      var contractBalance = 0
      const contractBalanceURL = 'https://api.etherscan.io/api?module=account&action=balance&address='+contractAddress+'&apikey=YourApiKeyToken'
      $.get( contractBalanceURL, function (data) {
      	contractBalance = Number(web33.utils.fromWei(data.result, 'ether'))
      })

      apiETHUSD = 'https://api.kraken.com/0/public/Ticker?pair=ETHUSD'
      $.get( apiETHUSD, function (data) {
      	const ETHUSD = Number(data.result.XETHZUSD.b[0])
      	const totalUSD = Math.round(total * ETHUSD * 100) / 100
	    $('.invest-counter-statistic').append(
        '<h6 class="display-3 statistic-counter">' + Math.round(total * 100) / 100 + '<span class="label-eth">ETH</span></h6><span class="label">'+ totalUSD +' USD </span>'
      )

	    const diff = Math.round(((total * 0.84) - contractBalance) * 100000) / 100000
	    const diffUSD = Math.round(diff * ETHUSD * 100) / 100
        $('.withdraw-counter-statistic').append(
        	'<h6 class="display-3 statistic-counter">'+ diff +'<span class="label-eth">ETH</span></h6><span class="label">'+ diffUSD +' USD</span>'
        )
      })

      $('.people-counter-statistic').append(
        '<h6 class="display-3 statistic-counter">' + addressArray.length + '</h6>'
      )
    })

    const drawInvestmentChart = function(chartArray) {
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable(chartArray);

        var options = {
          hAxis: {title: ' ',  titleTextStyle: {color: 'blueviolet'}},
          vAxis: {title: ' ', titleTextStyle: {color: 'blueviolet'}, minValue: 0},
          colors: ['blueviolet', 'blue']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    }
})
