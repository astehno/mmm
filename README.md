### Install

1. Install Nodejs 4+
2. Install npm (should be in package with nodejs). Check it `npm -v`

```
npm run setup
```

### Production Mode 


1. Change index.php , set $mode = 'prod'

2.
```
npm run compile
```


### Debug Mode


1. Change index.php , set $mode = 'debug'